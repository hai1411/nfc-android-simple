package com.example.myapplication

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.nfc.tech.NfcF
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val nfcAdapter: NfcAdapter? by lazy {
        NfcAdapter.getDefaultAdapter(applicationContext)
    }
    private var intentFiltersArray = emptyArray<IntentFilter>()
    private var techListsArray = arrayOf(arrayOf<String>(NfcF::class.java.name))
    private lateinit var pendingIntent: PendingIntent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val intent = Intent(this, javaClass).apply {
            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
        val a = 0
        pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_MUTABLE
        )
        val ndef = IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED).apply {
            try {
                // addDataType("*/*")
            } catch (e: IntentFilter.MalformedMimeTypeException) {
                throw RuntimeException("fail", e)
            }
        }
        intentFiltersArray = arrayOf(ndef)
        receiveIntentFromNFC(getIntent())
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.d("HaiLS", "Hello")
        intent?.let {
            receiveIntentFromNFC(it)
        }
    }

    private fun receiveIntentFromNFC(intent: Intent) {
        if (nfcAdapter != null) {
            if (NfcAdapter.ACTION_TAG_DISCOVERED == intent.action || NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action || NfcAdapter.ACTION_TECH_DISCOVERED == intent.action) {
                intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
                    ?.let { rawMessage ->
                        val messages: List<NdefMessage> = rawMessage.map { it as NdefMessage }
                        messages.forEach { it ->
                            it.records.forEach {
                                val payloadBytes = it.payload
                                val payloadText = String(
                                    payloadBytes,
                                    3,
                                    payloadBytes.size - 3,
                                    charset("UTF-8")
                                )
                                binding.tvTest.text = payloadText
                            }
                        }
                    }
            }
        } else {
            Toast.makeText(
                applicationContext,
                "Your device does not support NFC Reader",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter?.enableForegroundDispatch(
            this,
            pendingIntent,
            intentFiltersArray,
            techListsArray
        )
    }
}
